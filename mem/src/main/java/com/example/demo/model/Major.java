package com.example.demo.model;

import lombok.Data;

import javax.annotation.sql.DataSourceDefinitions;

@Data
public class Major {
    private String major_no;

    private String GB_major_no;

    private String major_name;

    private String en_major_name;

    private Byte length_school;

    private String edu_level;

    private String ddegree;

    private String department_no;

    private String department;
}