package com.example.demo.model;

import lombok.Data;

import javax.annotation.sql.DataSourceDefinitions;

@Data
public class SelectCourseKey {
    private String sno;

    private Byte school_year;

    private Byte semester;

    private String course_no;


}