package com.example.demo.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Course extends CourseKey {
    private String course_name;

    private BigDecimal credit;

    private Byte credit_hourse;

    private String course_type_1;

    private String course_type_2;

    private String cegment_type;

    private String examine_way;
}