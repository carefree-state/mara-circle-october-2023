package com.example.demo.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.Date;

@Data
public class Student {
    private String sno;

    private String sname;

    private String sex;

    private Date birthday;

    private String nationality;

    @SerializedName("native")
    private String nation;
    private String political;

    private String district;

    private String student_source;

    private Date enter_year;

    private Byte school_year;

    @SerializedName("class")
    private String classname;

    private String major_no;
}