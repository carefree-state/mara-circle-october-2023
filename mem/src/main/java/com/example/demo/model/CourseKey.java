package com.example.demo.model;

import lombok.Data;

@Data
public class CourseKey {
    private Byte school_year;

    private Byte semester;

    private String course_no;
}