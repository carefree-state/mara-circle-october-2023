package com.example.demo.dao;

import com.example.demo.model.Course;
import com.example.demo.model.CourseKey;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CourseMapper {
    int insert(Course row);

    int insertSelective(Course row);

    Course selectByPrimaryKey(CourseKey key);

    int updateByPrimaryKeySelective(Course row);

    int updateByPrimaryKey(Course row);
}