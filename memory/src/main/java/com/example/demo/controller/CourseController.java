package com.example.demo.controller;

import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import com.example.demo.config.Constants;
import com.example.demo.model.Course;
import com.example.demo.model.vo.ExcelCourseVo;
import com.example.demo.service.ICourseService;
import com.example.demo.utils.BeanCopyUtils;
import com.example.demo.utils.ExcelUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-12
 * Time: 11:17
 */

@Api(tags = "课程相关接口")
@Slf4j
@RestController
@RequestMapping("/course")
public class CourseController {

    @Resource
    private ICourseService courseService;
    @Resource
    private ObjectMapper objectMapper;

    @PostMapping("/insert")
    @ApiOperation("插入课程表格")
    public void insert(@NonNull @RequestParam("path") @ApiParam("表格路径") String path) throws JsonProcessingException {
        String json = ExcelUtils.xlsxTransferJson(path);
        List<Course> list = objectMapper.readValue(json, new TypeReference<List<Course>>() {});
        courseService.insert(list);
    }

    @GetMapping("/print")
    @ApiOperation("打印表格")
    public void print() {
        List<Course> list = courseService.selectAll();
        List<ExcelCourseVo> excelCourseVos = BeanCopyUtils.copyBeanList(list, ExcelCourseVo.class);
        // 导出
        ExportParams params = new ExportParams();
        params.setTitle("测试");
        // 表格左下角sheet名称
        params.setSheetName("课程信息");
        Workbook workbook = ExcelExportUtil.exportExcel(params, Course.class, list);
        try{
            // 文件夹是否存在，若没有对应文件夹直接根据路径生成文件会报错
            File folder = new File(Constants.XLSX_DIR);
            if (!folder.exists() && !folder.isDirectory()) {
                folder.mkdirs();
            }
            // 文件是否存在
            File file = new File(Constants.XLSX_DIR + "\\" + Constants.XLSX_NAME1);
            if (!file.exists()){
                file.createNewFile();
            }
            // 输出流写入(覆盖)
            FileOutputStream outputStream = new FileOutputStream(Constants.XLSX_DIR + "\\" + Constants.XLSX_NAME1);
            workbook.write(outputStream);
            // 关闭写，不然用户点击生成的文件会显示只读
            outputStream.close();
            workbook.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("打印成功");
    }
}

