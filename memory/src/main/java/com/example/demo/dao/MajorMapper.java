package com.example.demo.dao;

import com.example.demo.model.Course;
import com.example.demo.model.Major;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MajorMapper {
    int insert(Major row);

    int insertSelective(Major row);

    Major selectByPrimaryKey(String major_no);

    int updateByPrimaryKeySelective(Major row);

    int updateByPrimaryKey(Major row);


    @Select("select * from major")
    List<Major> selectAll();
}