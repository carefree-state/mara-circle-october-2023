package com.example.demo.dao;

import com.example.demo.model.Major;
import com.example.demo.model.SelectCourse;
import com.example.demo.model.SelectCourseKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SelectCourseMapper {
    int insert(SelectCourse row);

    int insertSelective(SelectCourse row);

    SelectCourse selectByPrimaryKey(SelectCourseKey key);

    int updateByPrimaryKeySelective(SelectCourse row);

    int updateByPrimaryKey(SelectCourse row);

    @Select("select * from select_course")
    List<SelectCourse> selectAll();
}