package com.example.demo.dao;

import com.example.demo.model.SelectCourse;
import com.example.demo.model.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface StudentMapper {
    int insert(Student row);

    int insertSelective(Student row);

    Student selectByPrimaryKey(String sno);

    int updateByPrimaryKeySelective(Student row);

    int updateByPrimaryKey(Student row);

    @Select("select * from student")
    List<Student> selectAll();
}