package com.example.demo.service.impl;

import com.example.demo.controller.SelectCourseController;
import com.example.demo.dao.MajorMapper;
import com.example.demo.dao.SelectCourseMapper;
import com.example.demo.model.Major;
import com.example.demo.model.SelectCourse;
import com.example.demo.service.ISelectCourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-12
 * Time: 13:47
 */
@Slf4j
@Service
public class SelectCourseServiceImpl implements ISelectCourseService {
    @Resource
    private SelectCourseMapper selectCourseMapper;

    @Override
    public void insert(List<SelectCourse> list) {
        for (int i = 0; i < list.size(); i++) {
            try {
                selectCourseMapper.insertSelective(list.get(i));
            }catch (RuntimeException e) {
                System.out.println("-----------------------------------------");
                log.error(i + "插入失败");
                System.out.println("-----------------------------------------");
            }
        }
    }

    @Override
    public List<SelectCourse> selectAll() {

        return selectCourseMapper.selectAll();

    }
}
