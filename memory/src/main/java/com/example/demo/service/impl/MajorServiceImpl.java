package com.example.demo.service.impl;

import com.example.demo.dao.MajorMapper;
import com.example.demo.model.Major;
import com.example.demo.service.IMajorService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-12
 * Time: 13:47
 */
@Slf4j
@Service
public class MajorServiceImpl implements IMajorService {
    @Resource
    private MajorMapper majorMapper;

    @Override
    public void insert(List<Major> list) {
        for (int i = 0; i < list.size(); i++) {
            try {
                majorMapper.insertSelective(list.get(i));
            }catch (RuntimeException e) {
                System.out.println("-----------------------------------------");
                log.error(i + "插入失败");
                System.out.println("-----------------------------------------");
            }
        }
    }

    @Override
    public List<Major> selectAll() {

        return majorMapper.selectAll();

    }
}
