package com.example.demo.service;

import com.example.demo.model.Course;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-12
 * Time: 11:19
 */
public interface ICourseService {

    void insert(List<Course> list);

    List<Course> selectAll();

}
