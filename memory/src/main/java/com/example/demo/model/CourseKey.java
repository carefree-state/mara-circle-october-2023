package com.example.demo.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ExcelTarget("CourseKey")
public class CourseKey {
    @Excel(name = "schoolYear", width = 20)
    private Integer school_year;

    @Excel(name = "semester", width = 20)
    private Integer semester;

    @Excel(name = "courseNo", width = 20)
    private String course_no;

}