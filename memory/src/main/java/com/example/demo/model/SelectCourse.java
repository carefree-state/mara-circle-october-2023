package com.example.demo.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class SelectCourse extends SelectCourseKey {
    @Excel(name = "score", width = 20)
    private BigDecimal score;
}