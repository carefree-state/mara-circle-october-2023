package com.example.demo.model.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-13
 * Time: 18:27
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ExcelTarget("excelCourse")
public class ExcelCourseVo {

    @Excel(name = "course", width = 20)
    private String course_name;

    @Excel(name = "credit", width = 20)
    private BigDecimal credit;

    @Excel(name = "school", width = 20)
    private Integer school_year;

    @Excel(name = "semester", width = 20)
    private Integer semester;
}
