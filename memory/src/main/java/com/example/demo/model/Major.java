package com.example.demo.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Major {
    @Excel(name = "majorNo", width = 20)
    private String major_no;

    @SerializedName("gb_major_no")
    @Excel(name = "gbMajorNo", width = 20)
    private String gb_major_no;

    @Excel(name = "majorName", width = 20)
    private String major_name;

    @Excel(name = "enMajorName", width = 20)
    private String en_major_name;

    @Excel(name = "lengthSchool", width = 20)
    private Integer length_school;

    @Excel(name = "eduLevel", width = 20)
    private String edu_level;

    @Excel(name = "ddegree", width = 20)
    private String ddegree;

    @Excel(name = "departmentNo", width = 20)
    private String department_no;

    @Excel(name = "department", width = 20)
    private String department;

 }