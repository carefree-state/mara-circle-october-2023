package com.example.demo.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.util.Date;

@Data
public class Student {
    @Excel(name = "sno", width = 20)
    private String sno;

    @Excel(name = "sname", width = 20)
    private String sname;

    @Excel(name = "sex", width = 20)
    private String sex;

    @Excel(name = "birthday", width = 20)
    private Date birthday;

    @Excel(name = "nationality", width = 20)
    private String nationality;

    @SerializedName("native")
    @Excel(name = "nation", width = 20)
    private String nation;

    @Excel(name = "political", width = 20)
    private String political;

    @Excel(name = "district", width = 20)
    private String district;

    @Excel(name = "studentSource", width = 20)
    private String student_source;

    @Excel(name = "enterYear", width = 20)
    private Date enter_year;

    @Excel(name = "schoolYear", width = 20)
    private Integer school_year;

    @SerializedName("class")
    @Excel(name = "classname", width = 20)
    private String classname;

    @Excel(name = "majorNo", width = 20)
    private String major_no;
}