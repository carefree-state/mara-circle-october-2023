package com.example.demo.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

@Data
public class SelectCourseKey {
    @Excel(name = "sno", width = 20)
    private String sno;

    @Excel(name = "schoolYear", width = 20)
    private Integer school_year;

    @Excel(name = "semester", width = 20)
    private Integer semester;

    @Excel(name = "courseNo", width = 20)
    private String course_no;
}