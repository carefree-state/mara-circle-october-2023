package com.example.demo.model;

import cn.afterturn.easypoi.excel.annotation.Excel;
import cn.afterturn.easypoi.excel.annotation.ExcelIgnore;
import cn.afterturn.easypoi.excel.annotation.ExcelTarget;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ExcelTarget("Course") // 表名不能带下划线（目前只知道不支持这个！）
public class Course extends CourseKey implements Serializable {

    @Excel(name = "courseName", width = 20)
    @ExcelIgnore
    private String course_name;

    @Excel(name = "credit", width = 20)
    @ExcelIgnore
    private BigDecimal credit;

    @Excel(name = "creditHourse", width = 20)
    @ExcelIgnore
    private Integer credit_hourse;

    @Excel(name = "courseType1", width = 20)
    private String course_type_1;

    @Excel(name = "courseType2", width = 20)
    private String course_type_2;

    @Excel(name = "cegmentType", width = 20)
    private String cegment_type;

    @Excel(name = "examineWay", width = 20)
    private String examine_way;


    @Override
    public String toString() {
        return "Course{" +
                "course_name='" + course_name + '\'' +
                ", credit=" + credit +
                ", credit_hourse=" + credit_hourse +
                ", course_type_1='" + course_type_1 + '\'' +
                ", course_type_2='" + course_type_2 + '\'' +
                ", cegment_type='" + cegment_type + '\'' +
                ", examine_way='" + examine_way + '\'' +
                '}' + super.toString();
    }
}