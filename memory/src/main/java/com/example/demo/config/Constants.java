package com.example.demo.config;

public class Constants {

    // excel表
    public static final String XLSX_DIR = "D:\\study\\excel";

    public static final String XLSX_NAME1 = "easypoi1.xlsx";
    public static final String XLSX_NAME2 = "easypoi2.xlsx";
    public static final String XLSX_NAME3 = "easypoi3.xlsx";
    public static final String XLSX_NAME4 = "easypoi4.xlsx";

}
