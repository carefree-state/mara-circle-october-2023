
-- 创建数据库
drop database if exists ma_ming_sheng;
CREATE database ma_ming_sheng  character set utf8mb4 collate utf8mb4_general_ci;
use ma_ming_sheng;

-- 专业设置
drop TABLE if exists major;
CREATE TABLE major (
    major_no char(4) NOT NULL primary key COMMENT '专业代码',
    GB_major_no char(6) NOT NULL COMMENT '国家专业编号',
    major_name VARCHAR(60) NOT NULL COMMENT '专业名称',
    en_major_name  VARCHAR(250) NOT NULL COMMENT '英文名称',
    length_school int NOT NULL DEFAULT 4 COMMENT '学制',
    edu_level char(6) NOT NULL DEFAULT '本科' COMMENT '培养层次',
    ddegree CHAR(12) NOT NULL COMMENT '授予学位',
    department_no CHAR(2) NOT NULL COMMENT '院系代码',
    department VARCHAR(40) NOT NULL COMMENT '院系名称'
);

-- 学生
drop TABLE if exists student;
CREATE TABLE student (
    sno char(12) NOT NULL primary key COMMENT '学生学号',
    sname char(16) NOT NULL COMMENT '学生姓名',
    sex  CHAR(2) NOT NULL DEFAULT '男' COMMENT '性别',
    birthday DATE NOT NULL COMMENT '出生日期',
    nationality  char(16) DEFAULT '汉族' COMMENT '民族',
    native VARCHAR(16) DEFAULT '东莞市' COMMENT '籍贯',
    political CHAR(12) DEFAULT '共青团员' COMMENT '政治面貌',
    district CHAR(12) NOT NULL DEFAULT '松山湖校区' COMMENT '院系代码',
    student_source VARCHAR(24) COMMENT '生源地',
    enter_year DATE NOT NULL COMMENT '入学日期',
    school_year int NOT NULL COMMENT '年级',
    class char(24) NOT NULL COMMENT '班级',
    major_no CHAR(4) NOT NULL COMMENT '专业代码',
    FOREIGN key (major_no) REFERENCES major(major_no) -- 设置副键
);

-- 课程设置
drop TABLE if EXISTS course;
CREATE TABLE course (
    school_year INT NOT NULL COMMENT '学年',
    semester  INT NOT NULL COMMENT '学期',
    course_no CHAR(8) NOT NULL COMMENT '课程代码',
    course_name VARCHAR(40) NOT NULL COMMENT '课程名称',
    credit NUMERIC(3,1) NOT NULL COMMENT '学分',
    credit_hourse  INT NOT NULL COMMENT '学时',
    course_type_1  CHAR(16) NOT NULL COMMENT '课程类别',
    course_type_2  CHAR(16) COMMENT '课程性质',
    cegment_type   CHAR(16) COMMENT '环节类别',
    examine_way   CHAR(6) COMMENT '考核方式',
    primary key (school_year, semester, course_no)
);

-- 学生选课
drop TABLE if exists select_course;
CREATE TABLE select_course (
    sno CHAR(12) NOT NULL COMMENT '学号',
    school_year int NOT NULL COMMENT '学年',
    semester  int NOT NULL COMMENT '学期',
    course_no CHAR(8) NOT NULL COMMENT '课程代码',
    score NUMERIC(6,2) COMMENT '综合成绩',
    primary key (sno, school_year, semester, course_no),
    FOREIGN key (sno) REFERENCES student(sno), -- 设置副键
    FOREIGN key (school_year, semester, course_no) REFERENCES course(school_year, semester, course_no) -- 设置副键
);













