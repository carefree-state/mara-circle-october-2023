import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-12
 * Time: 0:14
 */
public class Main {
    /**
     * excel中的数据转换为JSON格式 手动写入excel地址，直接在本地跑方法
     */
    public static StringBuilder excelByPath() throws Exception {
        StringBuilder excelToJson = new StringBuilder();
        //excel文件地址
        File file = new File("C:\\Users\\xxx\\Desktop\\test.xlsx");
        if (!file.exists()){
            throw new Exception("文件不存在!");
        }
        InputStream in = new FileInputStream(file);

        // 读取整个Excel
        XSSFWorkbook sheets = new XSSFWorkbook(in);
        // 获取第一个表单Sheet
        XSSFSheet sheetAt = sheets.getSheetAt(0);

        //默认第一行为标题行，i = 0
        XSSFRow titleRow = sheetAt.getRow(0);

        // 循环获取每一行数据
        for (int i = 1; i < sheetAt.getPhysicalNumberOfRows(); i++) {
            XSSFRow row = sheetAt.getRow(i);
            // 读取每一格内容
            StringBuilder sbCell = new StringBuilder();
            StringBuilder sbTitle = new StringBuilder();
            StringBuilder sbJson = new StringBuilder();
            for (int index = 0; index < row.getPhysicalNumberOfCells(); index++) {
                //title
                XSSFCell titleCell = titleRow.getCell(index);
                XSSFCell cell = row.getCell(index);
                titleCell.setCellType(CellType.STRING);
                cell.setCellType(CellType.STRING);
                if (cell.getStringCellValue().equals("")) {
                    continue;
                }
                sbTitle.append(titleCell);
                sbCell.append(cell);
                if (index == row.getPhysicalNumberOfCells() -1) {
                    sbJson.append("\t" + "\""+ titleCell + "\":" + "\"" + cell + "\"" );
                } else {
                    sbJson.append("\t" + "\""+ titleCell + "\":" + "\"" + cell + "\",\n" );
                }

            }
            if (i == 1) {
                excelToJson.append("[{\n" + sbJson + "\n},");
            } else if (i == sheetAt.getPhysicalNumberOfRows()-1) {
                excelToJson.append("{\n" + sbJson + "\n}\n]");
            } else {
                excelToJson.append("{\n" + sbJson + "\n},");
            }
        }
        return excelToJson;
    }
    /**
     *     输出字符串另存为txt文件
     */
    public static void saveAsTxt() throws Exception {
        //文件保存地址
        File outFile = new File("C:\\Users\\xxx\\Desktop\\" + DateUtils.getCurrentTime(DATETIME_FORMAT_STR) + ".txt");

        //检查文件是否存在，如果存在退出，注释掉这一段将实现重写覆盖
        if (outFile.exists()) {
            System.out.println("File already exists!");
            System.exit(1);
        }

        //如果不存在则写入文件
        // 制定编码方式为 UTF-8
        PrintWriter printWriter = new PrintWriter(outFile, "UTF-8");
        StringBuilder stringBuilder = excelByPath();
        printWriter.print(stringBuilder);

        //释放资源
        printWriter.close();
    }


}
