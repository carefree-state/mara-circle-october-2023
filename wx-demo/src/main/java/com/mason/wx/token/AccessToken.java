package com.mason.wx.token;

import com.mason.wx.utils.TokenUtils;
import lombok.Data;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-20
 * Time: 22:21
 */
@Data
public class AccessToken {

    private String token;

    private long expireTime;//有效期限


    volatile private static AccessToken accessToken = null;

    public void setExpireTime(long expireIn) {
        // 设置有效期限的时候的时间戳
        this.expireTime = System.currentTimeMillis() + expireIn * 1000;
    }

    public boolean isExpired() {
        return System.currentTimeMillis() > this.getExpireTime();
    }


    private static void setAccessToken() {
        if(accessToken == null) {
            accessToken = new AccessToken();
        }
        Map<String, Object> map = TokenUtils.getAccessTokenMap();
        accessToken.setToken((String) map.get("access_token"));
        accessToken.setExpireTime((Integer) map.get("expires_in"));
    }

    public static AccessToken getAccessToken() {
        if(accessToken == null || accessToken.isExpired()) {
            synchronized (AccessToken.class) {
                if(accessToken == null || accessToken.isExpired()) {
                    setAccessToken();
                }
            }
        }
        return accessToken;
    }

}
