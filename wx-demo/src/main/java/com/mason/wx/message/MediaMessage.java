package com.mason.wx.message;

import com.mason.wx.utils.HttpUtils;
import com.mason.wx.utils.JsonUtils;
import com.mason.wx.utils.TokenUtils;
import net.sf.json.JSONArray;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 19:29
 */
public class MediaMessage {

    public static String sendImage() {

        // 构造url
        String url = "https://api.weixin.qq.com/cgi-bin/media/upload" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken());
            this.put("type", "image");
        }});
        // 发起请求
        String response = HttpUtils.doPostByFile(url, null, "D:/马图/瞪眼.jpg", "");
        System.out.println(response);
        return (String) JsonUtils.jsonToMap(response).get("media_id");
    }

    public static String getImage(String mediaId) {
        // 构造url
        String url = "https://api.weixin.qq.com/cgi-bin/media/get" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken());
            this.put("media_id", mediaId);
        }});
        System.out.println(url);
        return HttpUtils.doGet(url, null);
    }

    public static void main(String[] args) {
        String ret = getImage(sendImage());
    }

}
