package com.mason.wx.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-20
 * Time: 16:41
 */
@Data
@XStreamAlias("item")
public class Article {

    @XStreamAlias("Title")
    private String title;

    @XStreamAlias ("Description")
    private String description;

    @XStreamAlias("PicUrl")
    private String picUrl;

    @XStreamAlias ("Url")
    private String url;
}
