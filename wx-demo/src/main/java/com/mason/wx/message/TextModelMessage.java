package com.mason.wx.message;

import com.mason.wx.utils.HttpUtils;
import com.mason.wx.utils.JsonUtils;
import com.mason.wx.utils.TokenUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 16:37
 */
public class TextModelMessage {

    public static void setTradeInfo() {
        String url = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken());
        }});
        Map<String, Object> param = new HashMap<String, Object>() {{
            this.put("industry_id1", "1");
            this.put("industry_id2", "2");
        }};
        System.out.println(HttpUtils.doPost(url, JsonUtils.objectToJson(param)));
    }

    public static String getTradeInfo() {
        String url = "https://api.weixin.qq.com/cgi-bin/template/get_industry" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken());
        }});
        return HttpUtils.doGet(url, null);
    }

    public static void sendModelMessage() {
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken());
        }});

        String data =
                "{" +
                "    \"first\":{\n" +
                "         \"value\":\"你好，你申请参加活动报名成功。\",\n" +
                "         \"color\":\"#101010\"" +
                "     },\n" +
                "     \"keyword1\":{\n" +
                "         \"value\":\"张三\",\n" +
                "         \"color\":\"#101010\"" +
                "     },\n" +
                "     \"keyword2\":{\n" +
                "         \"value\":\"13333333333\"\n" +
                "     },\n" +
                "     \"keyword3\": {\n" +
                "         \"value\":\"2023-10-21 17:43\"\n" +
                "     },\n" +
                "     \"keyword4\": {\n" +
                "         \"value\":\"你选择的是足球队员\"\n" +
                "     },\n" +
                "     \"remark\": {\n" +
                "         \"value\":\"感谢您 的使用，祝你生活愉快！\",\n" +
                "         \"color\":\"#FF0000\"" +
                "     }" +
                "}";
        Map<String, Object> param = new HashMap<String, Object>() {{
            this.put("touser", "otfI46nw4BoHVoOjivoWmEamB494");
            this.put("template_id", "jDrr4sGQBOgI7uTliXajxbaTTXMxhf2RzTXlwq3DBWY");
            this.put("url", "https://blog.csdn.net/Carefree_State?type=blog");
            this.put("data", JsonUtils.jsonToMap(data));
        }};
        // 发送请求
        System.out.println(HttpUtils.doPost(url, JsonUtils.objectToJson(param)));
    }


    public static void main(String[] args) {
//        setTradeInfo();
//        System.out.println(getTradeInfo());
        sendModelMessage();
    }

}
