package com.mason.wx.message;

import com.mason.wx.utils.HttpUtils;
import com.mason.wx.utils.WordUtils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-19
 * Time: 0:37
 */

@Data
@XStreamAlias("xml")
public class TextMessage {
    @XStreamAlias("ToUserName")
    private String toUserName;

    @XStreamAlias("FromUserName")
    private String fromUserName;

    @XStreamAlias("CreateTime")
    private long createTime;

    @XStreamAlias("MsgType")
    private String msgType;

    @XStreamAlias("Content")
    private String content;

    /**
     * 将map转换为对象返回
     * @param map 基本消息
     * @return
     */
    public static TextMessage getReplyTextMessage(Map<String, Object> map) {
        TextMessage message = new TextMessage();
        message.setToUserName((String) map.get("FromUserName"));
        message.setFromUserName((String) map.get("ToUserName"));
        message.setCreateTime(System.currentTimeMillis() / 1000);
        message.setMsgType("text");
        message.setContent((String) map.get("Content"));
        return message;
    }

    /**
     * 将TextMessage对象序列化为xml字符串返回
     * @param message
     * @return
     */
    public static String getXML(TextMessage message) {
        //获取序列化工具XStream对象
        XStream xStream = new XStream();
        //指定类型
        xStream.processAnnotations(TextMessage.class);
        //转化为xml字符串
        String xml = xStream.toXML(message);
        //返回
        return xml;
    }

    /**
     * 获取同义词
     * @param map
     * @return
     */
    public static TextMessage getSynonym(Map<String, Object> map) {
        TextMessage message = new TextMessage();
        message.setToUserName((String) map.get("FromUserName"));
        message.setFromUserName((String) map.get("ToUserName"));
        message.setCreateTime(System.currentTimeMillis() / 1000);
        message.setMsgType("text");
        List<String> words = WordUtils.getWords(1, (String) map.get("Content"));
        if(words == null || words.size() == 0) {
            return getReplyTextMessage(map);
        }else {
            message.setContent("回复同义词:  " + words);
            return message;
        }
    }

    /**
     * 获取反义词
     * @param map
     * @return
     */
    public static TextMessage getAntonym(Map<String, Object> map) {
        TextMessage message = new TextMessage();
        message.setToUserName((String) map.get("FromUserName"));
        message.setFromUserName((String) map.get("ToUserName"));
        message.setCreateTime(System.currentTimeMillis() / 1000);
        message.setMsgType("text");
        List<String> words = WordUtils.getWords(2, (String) map.get("Content"));
        if(words == null || words.size() == 0) {
            return getReplyTextMessage(map);
        }else {
            message.setContent("回复反义词:  " + words);
            return message;
        }
    }

}
