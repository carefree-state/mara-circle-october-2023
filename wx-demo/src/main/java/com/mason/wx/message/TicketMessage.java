package com.mason.wx.message;

import com.mason.wx.utils.HttpUtils;
import com.mason.wx.utils.JsonUtils;
import com.mason.wx.utils.TokenUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 21:04
 */
public class TicketMessage {

    public static String createTicket() {
        String url = "https://api.weixin.qq.com/cgi-bin/qrcode/create" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken());
        }});
        //生成临时二维码的数据
//        Map<String, Object> param = new HashMap<String, Object>(){{
//
//        }};
        String data = "{\"expire_seconds\": 3600, \"action_name\": \"QR_STR_SCENE\", \"action_info\": {\"scene\": {\"scene_str\": \"test\"}}";
        // 发送请求
        String ret = HttpUtils.doPost(url, data);
        System.out.println(ret);
        return (String) JsonUtils.jsonToMap(ret).get("ticket");
    }

    public static String getTicket() {
        String url = "https://mp.weixin.qq.com/cgi-bin/showqrcode" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("ticket", createTicket());
        }});
//        System.out.println(HttpUtils.doGet(url, null));
        return url;
    }

    public static void main(String[] args) {
//        System.out.println(createTicket());
        System.out.println(getTicket());
    }

}
