package com.mason.wx.utils;

import com.mason.wx.message.TextMessage;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 2:06
 */
public class EventUtils {
    public static String handleClick(Map<String, Object> map) {
        String message = "";
        String key = (String) map.get("EventKey");
        switch (key) {
            case "1":
                map.put("Content","\"触发了点击事件，key = 1\"");
                break;
            case "2":
                map.put("Content","\"触发了点击事件，key = 2\"");
                break;
            case "3":
                map.put("Content","\"触发了点击事件，key = 3\"");
                break;
            default:
                break;
        }
        TextMessage textMessage = TextMessage.getReplyTextMessage(map);
        message = XmlUtils.objectToXml(textMessage);
        return message;
    }


    public static String handleScan(Map<String, Object> map) {
        map.put("Content", "欢迎光临！ " + map.get("FromUserName"));
        TextMessage textMessage = TextMessage.getReplyTextMessage(map);
        return XmlUtils.objectToXml(textMessage);
    }

    public static String handleSubscribe(Map<String, Object> map) {
        map.put("Content", "感谢关注！  " + map.get("FromUserName"));
        TextMessage textMessage = TextMessage.getReplyTextMessage(map);
        return XmlUtils.objectToXml(textMessage);
    }
}
