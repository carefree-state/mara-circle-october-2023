package com.mason.wx.utils;

import com.mason.wx.button.Button;
import com.mason.wx.button.ext.ClickButton;
import com.mason.wx.button.ext.PicPhotoOrAlbumButton;
import com.mason.wx.button.ext.SubButton;
import com.mason.wx.button.ext.ViewButton;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 0:48
 */
public class ButtonUtils {

    public static Button createButton() {
        Button button = new Button();
        button.setButton(new ArrayList<>());
        return button;
    }

    public static ClickButton createClickButton(String name, String key) {
        ClickButton clickButton = new ClickButton(name);
        clickButton.setKey(key);
        return clickButton;
    }

    public static ViewButton createViewButton(String name, String url) {
        ViewButton viewButton = new ViewButton(name);
        viewButton.setUrl(url);
        return viewButton;
    }

    public static SubButton createSubButton(String name) {
        SubButton subButton = new SubButton(name);
        subButton.setSub_button(new ArrayList<>());
        return subButton;
    }
    public static PicPhotoOrAlbumButton createPicPhotoOrAlbumButton(String name, String key) {
        PicPhotoOrAlbumButton picPhotoOrAlbumButton = new PicPhotoOrAlbumButton(name);
        picPhotoOrAlbumButton.setKey(key);
        return picPhotoOrAlbumButton;
    }



    public static void main(String[] args) {
        Button button = createButton();
        // 一级菜单的两个按钮
        button.getButton().add(createClickButton("mara\uD83D\uDE00😀😀😀", "1"));
        button.getButton().add(createViewButton("baidu\uD83D\uDE00", "https://www.baidu.com"));
        // 二级菜单
        SubButton subButton = createSubButton("更多\uD83D\uDE00");
        subButton.getSub_button().add(createClickButton("mason\uD83D\uDE00", "2"));
        subButton.getSub_button().add(createViewButton("blog\uD83D\uDE00", "https://blog.csdn.net/Carefree_State?type=blog"));
        subButton.getSub_button().add(createPicPhotoOrAlbumButton("上传图片\uD83D\uDE00", "3"));
        // 二级菜单加入到一级菜单中
        button.getButton().add(subButton);
//        System.out.println(button);
        String json = JsonUtils.objectToJson(button);
        System.out.println(json);
        // 构造url
        String url = " https://api.weixin.qq.com/cgi-bin/menu/create" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken());
        }});
        // 发送post请求
        String response = HttpUtils.doPost(url, json);
        System.out.println(response);
    }

}
