package com.mason.wx.utils;

import com.mason.wx.token.AccessToken;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-20
 * Time: 21:50
 */
public class TokenUtils {


    private static final String APP_ID = "wxdadd0122365919e8";

    private static final String APP_SECRET = "69fd4a3ad04167f288e49bea9dce3e45";

    public static Map<String, Object> getAccessTokenMap() {
        // 获取token的url
        final String URL = "https://api.weixin.qq.com/cgi-bin/token";
        // 获取token的grant_type
        final String GRANT_TYPE = "client_credential";
        // 构造参数表
        Map<String, Object> param = new HashMap<String, Object>(){{
            this.put("grant_type", GRANT_TYPE);
            this.put("appid", APP_ID);
            this.put("secret", APP_SECRET);
        }};
        // 发起get请求
        String response = HttpUtils.doGet(URL, param);
        // 解析json
        Map<String, Object> result = JsonUtils.jsonToMap(response);
//        System.out.println(result);
        return result;
    }

    public static String getToken() {
        return AccessToken.getAccessToken().getToken();
    }

    public static void main(String[] args) {
        System.out.println(getToken());
        System.out.println(getToken());
        System.out.println(getToken());
    }

}
