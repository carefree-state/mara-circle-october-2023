package com.mason.wx.utils;

import com.baidu.aip.ocr.AipOcr;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 15:04
 */
public class ImageUtils {

    //设置APPID/AK/SK
    public static final String APP_ID = "41481385";
    public static final String API_KEY = "AxQaGsnr5VbZpQQxQV1RyA9B";
    public static final String SECRET_KEY = "uLRq53nbs8Dd2A6T1UAI30jtWXQ7Sc0s";

    public static String getImageText(String picUrl) {
        // 初始化一个AipOcr
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        System.out.println(picUrl);
        // 调用接口
        JSONObject jsonObject = client.webImageUrl(picUrl, new HashMap<String, String>());
        // 解析json字符串
        Map<String, Object> map = JsonUtils.jsonToMap(jsonObject.toString());
        // 获取单词集
        List<Map<String, Object>> wordsResult = (List<Map<String, Object>>) map.get("words_result");
        if(wordsResult == null || wordsResult.size() == 0) {
            return "";
        }
        List<String> words = new ArrayList<>();
        for(Map<String, Object> m : wordsResult) {
            words.add((String) m.get("words"));
        }
        System.out.println(words);
        // 返回识别结果
        return words.toString();
    }


}
