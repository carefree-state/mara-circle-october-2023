package com.mason.wx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasonWxDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasonWxDemoApplication.class, args);
	}

}
