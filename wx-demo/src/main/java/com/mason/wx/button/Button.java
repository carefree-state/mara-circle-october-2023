package com.mason.wx.button;

import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 0:05
 */
@Data
public class Button {

    private List<AbstractButton> button;

}
