package com.mason.wx.button;

import lombok.Data;

import javax.annotation.security.DenyAll;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 0:06
 */
@Data
public abstract class AbstractButton {

    private String name;

    public AbstractButton(String name) {
        this.name = name;
    }

    public AbstractButton() {
    }

}
