package com.mason.wx.button.ext;

import com.mason.wx.button.AbstractButton;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 0:23
 */
@Data
public class ViewButton extends AbstractButton {

    private final String type = "view";

    private String url;

    public ViewButton(String name) {
        super(name);
    }
}
