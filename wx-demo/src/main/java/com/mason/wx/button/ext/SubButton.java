package com.mason.wx.button.ext;

import com.google.gson.annotations.SerializedName;
import com.mason.wx.button.AbstractButton;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 0:35
 */
@Data
public class SubButton extends AbstractButton {

    private List<AbstractButton> sub_button;


    public SubButton(String name) {
        super(name);
    }
}