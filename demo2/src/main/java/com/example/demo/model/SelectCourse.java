package com.example.demo.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SelectCourse extends SelectCourseKey {
    private BigDecimal score;
}