package com.example.demo.model;

import lombok.Data;

@Data
public class SelectCourseKey {
    private String sno;

    private Integer school_year;

    private Integer semester;

    private String course_no;
}