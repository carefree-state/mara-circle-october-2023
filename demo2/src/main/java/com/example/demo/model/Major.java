package com.example.demo.model;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class Major {
    private String major_no;

    @SerializedName("gb_major_no")
    private String gb_major_no;

    private String major_name;

    private String en_major_name;

    private Integer length_school;

    private String edu_level;

    private String ddegree;

    private String department_no;

    private String department;

 }