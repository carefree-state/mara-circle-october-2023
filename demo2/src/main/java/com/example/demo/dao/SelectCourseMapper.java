package com.example.demo.dao;

import com.example.demo.model.SelectCourse;
import com.example.demo.model.SelectCourseKey;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SelectCourseMapper {
    int insert(SelectCourse row);

    int insertSelective(SelectCourse row);

    SelectCourse selectByPrimaryKey(SelectCourseKey key);

    int updateByPrimaryKeySelective(SelectCourse row);

    int updateByPrimaryKey(SelectCourse row);
}