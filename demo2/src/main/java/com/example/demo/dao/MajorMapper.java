package com.example.demo.dao;

import com.example.demo.model.Major;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MajorMapper {
    int insert(Major row);

    int insertSelective(Major row);

    Major selectByPrimaryKey(String major_no);

    int updateByPrimaryKeySelective(Major row);

    int updateByPrimaryKey(Major row);
}