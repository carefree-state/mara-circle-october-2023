package com.example.demo.dao;

import com.example.demo.model.Student;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StudentMapper {
    int insert(Student row);

    int insertSelective(Student row);

    Student selectByPrimaryKey(String sno);

    int updateByPrimaryKeySelective(Student row);

    int updateByPrimaryKey(Student row);
}