package com.example.demo.service;

import com.example.demo.model.Major;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-12
 * Time: 13:45
 */
public interface IMajorService {

    void insert(List<Major>list);
}
