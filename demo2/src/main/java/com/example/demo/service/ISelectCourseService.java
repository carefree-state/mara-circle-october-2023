package com.example.demo.service;

import com.example.demo.model.Major;
import com.example.demo.model.SelectCourse;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-12
 * Time: 13:46
 */
public interface ISelectCourseService {

    void insert(List<SelectCourse> list);
}
