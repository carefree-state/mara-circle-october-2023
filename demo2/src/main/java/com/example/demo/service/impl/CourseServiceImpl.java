package com.example.demo.service.impl;

import com.example.demo.dao.CourseMapper;
import com.example.demo.model.Course;
import com.example.demo.service.ICourseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-12
 * Time: 11:20
 */
@Service
@Slf4j
public class CourseServiceImpl implements ICourseService {
    @Resource
    private CourseMapper courseMapper;


    @Override
    public void insert(List<Course> list) {
        for (int i = 0; i < list.size(); i++) {
            try {
                courseMapper.insertSelective(list.get(i));
            }catch (RuntimeException e) {
                System.out.println("-----------------------------------------");
                log.error(i + "插入失败");
                System.out.println("-----------------------------------------");
            }
        }
    }
}
