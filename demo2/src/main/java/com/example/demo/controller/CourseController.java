package com.example.demo.controller;

import com.example.demo.model.Course;
import com.example.demo.service.ICourseService;
import com.example.demo.utils.ExcelUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-12
 * Time: 11:17
 */

@Api(tags = "课程相关接口")
@Slf4j
@RestController
@RequestMapping("/course")
public class CourseController {

    @Resource
    private ICourseService courseService;
    @Resource
    private ObjectMapper objectMapper;

    @PostMapping("/insert")
    @ApiOperation("插入课程表格")
    public void insert(@NonNull @RequestParam("path") @ApiParam("表格路径") String path) throws JsonProcessingException {
        String json = ExcelUtils.xlsxTransferJson(path);
        List<Course> list = objectMapper.readValue(json, new TypeReference<List<Course>>() {});
        courseService.insert(list);
    }
}

