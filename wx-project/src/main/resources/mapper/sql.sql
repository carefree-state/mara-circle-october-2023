DROP database if EXISTS db_wx;

create database db_wx charset=utf8mb4;

use db_wx;

DROP TABLE if EXISTS t_user;

create TABLE t_user (
    openid VARCHAR(100) PRIMARY KEY,
    nickname VARCHAR(100),
    headimgurl VARCHAR(150)
);

DROP TABLE if EXISTS t_follow_info;

create TABLE t_follow_info (
    openid VARCHAR(100),
    follow_openid VARCHAR(100),
    follow_time datetime,
    PRIMARY KEY (openid, follow_openid)
);

DROP TABLE if EXISTS t_assist_level;

create TABLE t_assist_level (
    id int,
    assist_level varchar(10),
    assist_count int
);

DELETE from t_assist_level;

insert into t_assist_level values(1, "第一档", 80), (2, "第二档", 40), (1, "第三档", 20), (1, "第四档", 10);
