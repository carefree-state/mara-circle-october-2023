package com.example.demo.utils;

import com.example.demo.model.RankingItem;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-22
 * Time: 13:41
 */
public class HtmlUtils {

    public static String createHtml(List<RankingItem> rankingList) {
        String preHtml = "<!doctype html>\n" +
                "<html lang=\"zh-CN\">\n" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <title>助力排行榜</title>\n" +
                "    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\" integrity=\"sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu\" crossorigin=\"anonymous\">\n" +
                "  </head>\n" +
                "  <body>\n" +
                "    <table  width=\"90%\" align=\"center\" style=\"text-align: center;\"  class=\"table table-striped\">\n" +
                "\t\t<thead>\n" +
                "\t\t\t<tr>\n" +
                "\t\t\t\t<td>微信名</td>\n" +
                "\t\t\t\t<td>助力值</td>\n" +
                "\t\t\t</tr>\n" +
                "\t\t</thead>\n" +
                "      <tbody>";
        StringBuilder stringBuilder = new StringBuilder();
        for (RankingItem rankingItem : rankingList) {
            stringBuilder.append("<tr><td>");
            stringBuilder.append(rankingItem.getNickname());
            stringBuilder.append("</td><td>");
            stringBuilder.append(rankingItem.getCount());
            stringBuilder.append("</td></tr>");
        }
        String content = stringBuilder.toString();
        String afterHtml = "</tbody></table><script src=\"https://cdn.jsdelivr.cn/npm/jquery@1.12.4/dist/jquery.min.js\" integrity=\"sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ\" crossorigin=\"anonymous\"></script>\n" +
                "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\" integrity=\"sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd\" crossorigin=\"anonymous\"></script>\n" +
                "</body></html>";
        return preHtml+content+afterHtml;
    }

    public static String getSignUpHtml() {
        //return "<h2>报名成功，返回后请在公众号输入关键字：</h2><h1>海报</h1><h2>分享活动海报，邀请好友扫码助力，输入\"排行榜\"查看助力排行榜</h2>";
        String html = "<!doctype html>\n" +
                "<html lang=\"zh-CN\">\n" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                "    <title>活动详情</title>\n" +
                "    <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css\" integrity=\"sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu\" crossorigin=\"anonymous\">\n" +
                "  </head>\n" +
                "  <body>\n" +
                "\t  <div class=\"container\">\n" +
                "\t\t  <div class=\"row\">\n" +
                "\t\t\t  <blockquote>\n" +
                "\t\t\t    <p>恭喜你，报名成功！</p>\n" +
                "\t\t\t  \t\t<p>请关闭此页面后在公众号输入关键字：</p>\n" +
                "\t\t\t  \t\t<p><strong>海报</strong></p>\n" +
                "\t\t\t    <p>分享活动海报，邀请好友扫码助力，输入&nbsp;\"<strong>排行榜</strong>\"&nbsp;查看助力排行榜</p>\n" +
                "\t\t\t  </blockquote>\n" +
                "\t\t  </div>\n" +
                "\t  </div>\n" +
                "    <script src=\"https://cdn.jsdelivr.cn/npm/jquery@1.12.4/dist/jquery.min.js\" integrity=\"sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ\" crossorigin=\"anonymous\"></script>\n" +
                "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js\" integrity=\"sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd\" crossorigin=\"anonymous\"></script>\n" +
                "  </body>\n" +
                "</html>";
        return html;
    }

}
