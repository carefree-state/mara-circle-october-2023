package com.example.demo.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-20
 * Time: 15:16
 */
public class WordUtils {
    /**
     * 获取同义词/反义词
     *
     */
    public static List<String> getWords(Integer type, String word) {
        // 构造参数表
        Map<String, Object> params = new HashMap<>();
        params.put("key", HttpUtils.KEY);
        params.put("type", type);
        params.put("word", word);
        //发起请求接受响应
        final String response = HttpUtils.doPost(HttpUtils.URL, params);
//        System.out.println("接口返回：" + response);
        try {
            Map<String, Object> ret = JsonUtils.jsonToMap(response);
            int error_code = (Integer) ret.get("error_code");
            String reason = (String) ret.get("reason");
            if (error_code == 0) {
                System.out.println("调用接口成功");
                Map<String, Object> result = (Map<String, Object>) ret.get("result");
                String t = (String) result.get("type");//返回的type
                List<String> words = (List<String>) result.get("words");
//                System.out.println(t);
                System.out.println(words);
                return words;
            } else {
                System.out.println("调用接口失败：" + reason);
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
