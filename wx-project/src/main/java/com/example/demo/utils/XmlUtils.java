package com.example.demo.utils;

import com.thoughtworks.xstream.XStream;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.servlet.ServletInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-20
 * Time: 15:20
 */
public class XmlUtils {
    public static String objectToXml(Object o) {
        if(o == null) {
            return null;
        }
        //获取序列化工具XStream对象
        XStream xStream = new XStream();
        //指定类型
        xStream.processAnnotations(o.getClass());
        //转化为xml字符串
        String xml = xStream.toXML(o);
        //返回
        return xml;
    }

    public static Map<String, String> xmlToMap(ServletInputStream inputStream) {
        Map<String, String> map = new HashMap<>();
        SAXReader reader = new SAXReader();
        // xml字符串解析方法
        try {
            //通过请求的输入流，获取Document对象
            Document document = reader.read(inputStream);
            // 获取root节点
            Element root = document.getRootElement();
            // 获取所有子节点
            List<Element> elements = root.elements();
            // 遍历集合
            for(Element e : elements) {
                map.put(e.getName(), e.getStringValue());
            }
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
        return map;
    }

    public static Map<String, Object> xmlToMap(String xml) {
        Map<String, Object> map = new HashMap<>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            org.w3c.dom.Document document = builder.parse(new InputSource(new StringReader(xml)));
            org.w3c.dom.Element root = document.getDocumentElement();
            map = parseElement(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
    private static Map<String, Object> parseElement(org.w3c.dom.Element element) {
        Map<String, Object> map = new HashMap<>();
        NodeList nodeList = element.getChildNodes();

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                org.w3c.dom.Element childElement = (org.w3c.dom.Element) node;
                if (childElement.getChildNodes().getLength() > 1) {
                    map.put(childElement.getTagName(), parseElement(childElement));
                } else {
                    map.put(childElement.getTagName(), childElement.getTextContent());
                }
            }
        }
        return map;
    }
}
