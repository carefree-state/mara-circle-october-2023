package com.example.demo.utils;

import com.baidu.aip.ocr.AipOcr;
import com.example.demo.config.QRCodeConstant;
import com.freewayso.image.combiner.ImageCombiner;
import com.freewayso.image.combiner.enums.OutputFormat;
import com.freewayso.image.combiner.enums.ZoomMode;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 15:04
 */
public class ImageUtils {

    //设置APPID/AK/SK
    public static final String APP_ID = "41481385";
    public static final String API_KEY = "AxQaGsnr5VbZpQQxQV1RyA9B";
    public static final String SECRET_KEY = "uLRq53nbs8Dd2A6T1UAI30jtWXQ7Sc0s";

    public static String getImageText(String picUrl) {
        // 初始化一个AipOcr
        AipOcr client = new AipOcr(APP_ID, API_KEY, SECRET_KEY);
        System.out.println(picUrl);
        // 调用接口
        JSONObject jsonObject = client.webImageUrl(picUrl, new HashMap<String, String>());
        // 解析json字符串
        Map<String, Object> map = JsonUtils.jsonToMap(jsonObject.toString());
        // 获取单词集
        List<Map<String, Object>> wordsResult = (List<Map<String, Object>>) map.get("words_result");
        if(wordsResult == null || wordsResult.size() == 0) {
            return "";
        }
        List<String> words = new ArrayList<>();
        for(Map<String, Object> m : wordsResult) {
            words.add((String) m.get("words"));
        }
        System.out.println(words);
        // 返回识别结果
        return words.toString();
    }


    /**
     * 完整功能
     *
     * @param qrCodeUrl 二维码路径
     * @param headImgUrl 头像路径
     * @param wxId 微信号
     * @throws Exception
     * @return 图片保存路径
     */
    public static String MergeImage(String qrCodeUrl,String headImgUrl,String wxId) throws Exception {
        String bgImagePath = QRCodeConstant.DOWNLOAD_PATH_PREFIX+"bgimage2.jpeg";                       //背景图（测试url形式）
        BufferedImage avatar = ImageIO.read(new URL(headImgUrl));           //头像
        BufferedImage bgImage = ImageIO.read(new FileInputStream(bgImagePath));

        //合成器和背景图（整个图片的宽高和相关计算依赖于背景图，所以背景图的大小是个基准）
        ImageCombiner combiner = new ImageCombiner(bgImage, OutputFormat.PNG);
        combiner.setBackgroundBlur(0);     //设置背景高斯模糊（毛玻璃效果）
        combiner.setCanvasRoundCorner(0); //设置整图圆角（输出格式必须为PNG）

        //头像（圆角设置一定的大小，可以把头像变成圆的）
        combiner.addImageElement(avatar, 500, 1200, 186, 186, ZoomMode.WidthHeight)
                .setRoundCorner(10)
                .setBlur(0);       //高斯模糊，毛玻璃效果

        //二维码（强制按指定宽度、高度缩放）
        combiner.addImageElement(ImageIO.read(new FileInputStream(qrCodeUrl)), 138, 1200, 186, 186, ZoomMode.WidthHeight).setRoundCorner(10);

        //执行图片合并
        combiner.combine();

        //保存文件
        String path = QRCodeConstant.DOWNLOAD_PATH_PREFIX+wxId+"_full.png";
        combiner.save(path);

        return path;
    }


}
