package com.example.demo.utils;


import com.example.demo.dao.TFollowInfoMapper;
import com.example.demo.message.TextMessage;
import com.example.demo.model.TFollowInfo;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 2:06
 */
public class EventUtils {
    public static String handleClick(Map<String, Object> map) {
        String message = "";
        String key = (String) map.get("EventKey");
        switch (key) {
            case "1":
                map.put("Content","\"触发了点击事件，key = 1\"");
                break;
            case "2":
                map.put("Content","\"触发了点击事件，key = 2\"");
                break;
            case "3":
                map.put("Content","\"触发了点击事件，key = 3\"");
                break;
            default:
                break;
        }
        TextMessage textMessage = TextMessage.getReplyTextMessage(map);
        message = XmlUtils.objectToXml(textMessage);
        return message;
    }


    public static String handleScan(Map<String, Object> map) {
        map.put("Content", "欢迎光临！ " + map.get("FromUserName"));
        TextMessage textMessage = TextMessage.getReplyTextMessage(map);
        return XmlUtils.objectToXml(textMessage);
    }

    public static String handleSubscribe(Map<String, Object> map) {
        map.put("Content", "感谢关注！  " + map.get("FromUserName"));
        TextMessage textMessage = TextMessage.getReplyTextMessage(map);
        return XmlUtils.objectToXml(textMessage);
    }

    public static TFollowInfo handleSubscribeByKey(Map<String, Object> map, String sharedUserWxId) {
        //解析map
        //EventKey:分享者id
        //将分享者信息和被关注者存入数据库的关注量表和关注记录表- t_follow_info

        //获得此次关注者的微信号（即此事件消息的发起者）
        String fromUserName = (String) map.get("FromUserName");
        // 构造一个分享与被分享的关系对象
        TFollowInfo followInfo = new TFollowInfo();
        followInfo.setOpenid(sharedUserWxId);
        followInfo.setFollow_openid(fromUserName);
        followInfo.setFollow_time(new Date());
        return followInfo;
    }
}
