package com.example.demo.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-22
 * Time: 14:15
 */
@Data
public class QRCode {
    private String action_name;
    private ActionInfo action_info;
}
