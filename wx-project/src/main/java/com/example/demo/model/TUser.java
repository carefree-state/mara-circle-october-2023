package com.example.demo.model;

import lombok.Data;

@Data
public class TUser {
    private String openid;

    private String nickname;

    private String headimgurl;

}