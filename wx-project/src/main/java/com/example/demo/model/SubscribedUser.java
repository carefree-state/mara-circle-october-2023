package com.example.demo.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-22
 * Time: 13:32
 */
@Data
public class SubscribedUser {
    private String openid;
    private String nickname;
    private int sex;
    private String city;
    private String province;
    private String country;
    private String headimgurl;
    private long subscribeTime;
}
