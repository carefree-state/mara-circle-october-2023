package com.example.demo.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-22
 * Time: 14:16
 */
@Data
public class Scene {
    private String scene_str;

    public Scene(String scene_str) {
        this.scene_str = scene_str;
    }
}
