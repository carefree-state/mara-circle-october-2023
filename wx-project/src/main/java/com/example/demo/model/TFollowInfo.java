package com.example.demo.model;

import lombok.Data;

import java.util.Date;

@Data
public class TFollowInfo extends TFollowInfoKey {
    private Date follow_time;

}