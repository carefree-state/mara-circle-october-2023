package com.example.demo.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-22
 * Time: 15:03
 */
@Data
public class RankingItem {

    private String nickname;
    //关注数
    private int count;
}
