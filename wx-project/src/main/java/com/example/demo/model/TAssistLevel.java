package com.example.demo.model;

import lombok.Data;

@Data
public class TAssistLevel {
    private Integer id;

    private String assist_level;

    private Integer assist_count;
}