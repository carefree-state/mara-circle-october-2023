package com.example.demo.model;


import lombok.Data;

@Data
public class TFollowInfoKey {
    private String openid;

    private String follow_openid;

}