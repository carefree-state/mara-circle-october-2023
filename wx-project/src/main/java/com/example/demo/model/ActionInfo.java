package com.example.demo.model;

import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-22
 * Time: 14:15
 */
@Data
public class ActionInfo {

    private Scene scene;

    public ActionInfo(Scene scene) {
        this.scene = scene;
    }
}
