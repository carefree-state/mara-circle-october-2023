package com.example.demo.button.ext;

import com.example.demo.button.AbstractButton;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 0:16
 */
@Data
public class ClickButton extends AbstractButton {

    private final String type = "click";

    private String key;

    public ClickButton(String name) {
        super(name);
    }

}
