package com.example.demo.button.ext;

import com.example.demo.button.AbstractButton;
import lombok.Data;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 0:30
 */
@Data
public class PicPhotoOrAlbumButton extends AbstractButton {

    private final String type = "pic_photo_or_album";

    private String key;


    public PicPhotoOrAlbumButton(String name) {
        super(name);
    }
}
