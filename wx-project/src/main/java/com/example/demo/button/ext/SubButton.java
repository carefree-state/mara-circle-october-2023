package com.example.demo.button.ext;

import com.example.demo.button.AbstractButton;
import lombok.Data;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 0:35
 */
@Data
public class SubButton extends AbstractButton {

    private List<AbstractButton> sub_button;


    public SubButton(String name) {
        super(name);
    }
}