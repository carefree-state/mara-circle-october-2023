package com.example.demo.controller;

import com.example.demo.service.WxService;
import com.example.demo.service.impl.WxServiceImpl;
import com.example.demo.utils.HttpUtils;
import com.example.demo.utils.XmlUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-22
 * Time: 12:16
 */

@RestController
@Slf4j
public class WxController {

    @Autowired
    private WxService wxService;

    @GetMapping("/")
    public String check(@RequestParam("signature") String signature,
                        @RequestParam("timestamp") String timestamp,
                        @RequestParam("nonce") String nonce,
                        @RequestParam("echostr") String echostr) {
        if(!StringUtils.hasLength(signature) || !StringUtils.hasLength(timestamp)
                || !StringUtils.hasLength(nonce) || !StringUtils.hasLength(echostr)) {
            return "";
        }
        return wxService.check(signature, timestamp, nonce, echostr);
    }
    @PostMapping("/")
    public String receiveMessage(HttpServletRequest request) throws IOException {
        String body = HttpUtils.getBody(request);
        Map<String, Object> map = XmlUtils.xmlToMap(body);
        System.out.println(map);
        // 获取回复消息
        return wxService.handleMessage(map);
    }

    //getSignUpUserInfo
    @RequestMapping("/getSignUpUserInfo")
    public String getSignUpUserInfo(HttpServletRequest request) {
        //获取code
        String code = request.getParameter("code");
        //这个code如果没有设置专门的url让用户点击，是没有的
        return wxService.getSignUpUserInfo(code);
    }

    /**
     * 获得排行榜
     * @return
     */
    @RequestMapping("/getRankingList")
    public String getRankingList(){
        return wxService.getRankingList();
    }



}
