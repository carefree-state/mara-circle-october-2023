package com.example.demo.token;

import com.example.demo.utils.HttpUtils;
import com.example.demo.utils.JsonUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-20
 * Time: 21:50
 */
public class TokenUtils {


//    private static final String APP_ID = "wxdadd0122365919e8";
//
//    private static final String APP_SECRET = "69fd4a3ad04167f288e49bea9dce3e45";

    public static Map<String, Object> getAccessTokenMap(String appId, String appSecret) {
        // 获取token的url
        final String URL = "https://api.weixin.qq.com/cgi-bin/token";
        // 获取token的grant_type
        final String GRANT_TYPE = "client_credential";
        // 构造参数表
        Map<String, Object> param = new HashMap<String, Object>(){{
            this.put("grant_type", GRANT_TYPE);
            this.put("appid", appId);
            this.put("secret", appSecret);
        }};
        // 发起get请求
        String response = HttpUtils.doGet(URL, param);
        // 解析json
        Map<String, Object> result = JsonUtils.jsonToMap(response);
//        System.out.println(result);
        return result;
    }

    public static String getToken(String appId, String appSecret) {
        return AccessToken.getAccessToken(appId, appSecret).getToken();
    }

    public static void main(String[] args) {
//        System.out.println(getToken());
//        System.out.println(getToken());
//        System.out.println(getToken());
    }

}
