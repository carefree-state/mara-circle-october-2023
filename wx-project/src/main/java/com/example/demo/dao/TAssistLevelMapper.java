package com.example.demo.dao;

import com.example.demo.model.TAssistLevel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TAssistLevelMapper {
    int insert(TAssistLevel row);

    int insertSelective(TAssistLevel row);

    List<TAssistLevel> selectAll();
}