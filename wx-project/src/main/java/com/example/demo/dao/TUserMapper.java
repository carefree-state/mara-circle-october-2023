package com.example.demo.dao;

import com.example.demo.model.TUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TUserMapper {
    int insert(TUser row);

    int insertSelective(TUser row);

    TUser selectByPrimaryKey(String openid);
}