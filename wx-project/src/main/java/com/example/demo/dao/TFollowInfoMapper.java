package com.example.demo.dao;

import com.example.demo.model.RankingItem;
import com.example.demo.model.TFollowInfo;
import com.example.demo.model.TFollowInfoKey;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface TFollowInfoMapper {
    int insert(TFollowInfo row);

    int insertSelective(TFollowInfo row);

    TFollowInfo selectByPrimaryKey(TFollowInfoKey key);

    int updateByPrimaryKeySelective(TFollowInfo row);

    int updateByPrimaryKey(TFollowInfo row);

    List<RankingItem> selectRankingList();

    int selectCountByOpenid(String openid);
}