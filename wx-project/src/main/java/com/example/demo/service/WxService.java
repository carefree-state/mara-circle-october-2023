package com.example.demo.service;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-22
 * Time: 12:16
 */
public interface WxService {

    String check(String signature, String timestamp, String nonce, String echostr);

    String handleMessage(Map<String, Object> map);

    String getSignUpUserInfo(String code);

    String getRankingList();
}
