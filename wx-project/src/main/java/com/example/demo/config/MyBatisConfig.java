package com.example.demo.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-01
 * Time: 21:59
 */
@Configuration
//配置扫描路径，提高效率
@MapperScan("com.example.demo.dao")
public class MyBatisConfig {

}
