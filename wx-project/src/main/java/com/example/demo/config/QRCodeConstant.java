package com.example.demo.config;

public interface QRCodeConstant {

    String ACTION_NAME="QR_LIMIT_STR_SCENE";

    String DOWNLOAD_PATH_PREFIX = "D:/";

    String DOWNLOAD_PATH_EXTENSION = ".jpeg";
}