package com.example.demo.message;


import com.example.demo.model.TAssistLevel;
import com.example.demo.utils.HtmlUtils;
import com.example.demo.utils.HttpUtils;
import com.example.demo.utils.JsonUtils;
import com.example.demo.token.TokenUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 16:37
 */
public class TextModelMessage {

    public static void setTradeInfo(String appId, String secret) {
        String url = "https://api.weixin.qq.com/cgi-bin/template/api_set_industry" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken(appId, secret));
        }});
        Map<String, Object> param = new HashMap<String, Object>() {{
            this.put("industry_id1", "1");
            this.put("industry_id2", "2");
        }};
        System.out.println(HttpUtils.doPost(url, JsonUtils.objectToJson(param)));
    }

    public static String getTradeInfo(String appId, String secret) {
        String url = "https://api.weixin.qq.com/cgi-bin/template/get_industry" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken(appId, secret));
        }});
        return HttpUtils.doGet(url, null);
    }

//    public static void sendModelMessage(String appId, String secret) {
//        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
//            this.put("access_token", TokenUtils.getToken(appId, secret));
//        }});
//
//        String data =
//                "{" +
//                "    \"first\":{\n" +
//                "         \"value\":\"你好，你申请参加活动报名成功。\",\n" +
//                "         \"color\":\"#101010\"" +
//                "     },\n" +
//                "     \"keyword1\":{\n" +
//                "         \"value\":\"张三\",\n" +
//                "         \"color\":\"#101010\"" +
//                "     },\n" +
//                "     \"keyword2\":{\n" +
//                "         \"value\":\"13333333333\"\n" +
//                "     },\n" +
//                "     \"keyword3\": {\n" +
//                "         \"value\":\"2023-10-21 17:43\"\n" +
//                "     },\n" +
//                "     \"keyword4\": {\n" +
//                "         \"value\":\"你选择的是足球队员\"\n" +
//                "     },\n" +
//                "     \"remark\": {\n" +
//                "         \"value\":\"感谢您 的使用，祝你生活愉快！\",\n" +
//                "         \"color\":\"#FF0000\"" +
//                "     }" +
//                "}";
//        Map<String, Object> param = new HashMap<String, Object>() {{
//            this.put("touser", "otfI46nw4BoHVoOjivoWmEamB494");
//            this.put("template_id", "jDrr4sGQBOgI7uTliXajxbaTTXMxhf2RzTXlwq3DBWY");
//            this.put("url", "https://blog.csdn.net/Carefree_State?type=blog");
//            this.put("data", JsonUtils.jsonToMap(data));
//        }};
//        // 发送请求
//        System.out.println(HttpUtils.doPost(url, JsonUtils.objectToJson(param)));
//    }
    public static void sendModelMessage(String sharedUserWxId, int count,
                                        List<TAssistLevel> assistLevels, String appId, String secret) {
        String url = "https://api.weixin.qq.com/cgi-bin/message/template/send" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken(appId, secret));
        }});
        //根据目前助力值获得返回内容
        String remark = getRemarkContent(count, assistLevels);
        // 获取html
        String data = "{\n" +
                "           \"touser\":\"" + sharedUserWxId + "\",\n" +
                "           \"template_id\":\"PTIeFp6JW8ytPtfVZZ1EOurKI_wycz1rLT0rZVDIgnI\",\n" +
                "           \"data\":{\n" +
                "                   \"keyword1\": {\n" +
                "                       \"value\":\"恭喜你！您得到了一位好友的支持！\",\n" +
                "                       \"color\":\"#173177\"\n" +
                "                   },\n" +
                "                   \"keyword2\":{\n" +
                "                       \"value\":\""+count+"\",\n" +
                "                       \"color\":\"#173177\"\n" +
                "                   },\n" +
                "                   \"keyword3\":{\n" +
                "                       \"value\":\""+remark+"\",\n" +
                "                       \"color\":\"#173177\"\n" +
                "                   }\n" +
                "           }\n" +
                "       }";
        HttpUtils.doPost(url, data);
    }


    private static String getRemarkContent(int count, List<TAssistLevel> assistLevels) {
        int level1 = 0;
        int level2 = 0;
        int level3 = 0;
        int level4 = 0;
        for (TAssistLevel assistLevel : assistLevels) {
            switch (assistLevel.getAssist_level()){
                case "第一档":
                    level1 = assistLevel.getAssist_count();
                    break;
                case "第二档":
                    level2 = assistLevel.getAssist_count();
                    break;
                case "第三档":
                    level3 = assistLevel.getAssist_count();
                    break;
                case "第四档":
                    level4 = assistLevel.getAssist_count();
                    break;
                default: break;
            }
        }
        String remark = "";
        if(count<level4){
            remark = "助力值目前处于第五档，距离第四档还差："+(level4-count)+"个";
        }else if(count>=level4 &&count<level3){
            remark = "助力值目前处于第四档，距离第三档还差："+(level3-count)+"个";
        }else if(count>=level3 &&count<level2){
            remark = "助力值目前处于第三档，距离第二档还差："+(level2-count)+"个";
        }else if(count>=level2 &&count<level1){
            remark = "助力值目前处于第二档，距离第一档还差："+(level1-count)+"个";
        }else {
            remark = "恭喜你，助力值处于第一档，继续分享，让自己摇摇领先！";
        }
        return remark;
    }

//
//    public static void main(String[] args) {
////        setTradeInfo();
////        System.out.println(getTradeInfo());
////        sendModelMessage();
//    }

}
