package com.example.demo.message;


import com.alibaba.fastjson.JSONObject;
import com.example.demo.utils.HttpUtils;
import com.example.demo.utils.JsonUtils;
import com.example.demo.token.TokenUtils;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-21
 * Time: 19:29
 */
public class MediaMessage {

    public static String sendImage(String mergeImgPath, String appId, String secret) {

        // 构造url
        String url = "https://api.weixin.qq.com/cgi-bin/media/upload" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken(appId, secret));
            this.put("type", "image");
        }});
        // 发起请求
        String response = HttpUtils.doPostByFile(url, null, mergeImgPath, "");
        System.out.println(response);
        return JSONObject.parseObject(response).getString("media_id");
    }

    public static String getImage(String mediaId, String appId, String secret) {
        // 构造url
        String url = "https://api.weixin.qq.com/cgi-bin/media/get" + HttpUtils.getQueryString(new HashMap<String, Object>() {{
            this.put("access_token", TokenUtils.getToken(appId, secret));
            this.put("media_id", mediaId);
        }});
        System.out.println(url);
        return HttpUtils.doGet(url, null);
    }

    public static void main(String[] args) {
//        String ret = getImage(sendImage());
    }

}
