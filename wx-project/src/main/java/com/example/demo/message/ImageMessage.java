package com.example.demo.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-22
 * Time: 14:53
 */

@XStreamAlias("xml")
@Data
public class ImageMessage {

    @XStreamAlias("ToUserName")
    private String toUserName;
    @XStreamAlias("FromUserName")
    private String fromUserName;
    @XStreamAlias("CreateTime")
    private Long createTime;
    @XStreamAlias("MsgType")
    private String msgType;
    @XStreamAlias("Image")
    private Image image;

    public static ImageMessage getImageMessage(Map<String, Object> map, String mediaId) {
        ImageMessage imageMessage = new ImageMessage();
        imageMessage.setToUserName((String) map.get("FromUserName"));
        imageMessage.setFromUserName((String) map.get("ToUserName"));
        imageMessage.setCreateTime(System.currentTimeMillis()/1000);
        imageMessage.setImage(new Image(mediaId));
        imageMessage.setMsgType("image");
        return imageMessage;
    }
}
