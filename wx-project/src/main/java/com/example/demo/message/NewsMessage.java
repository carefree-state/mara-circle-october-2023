package com.example.demo.message;

import com.example.demo.model.Article;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-10-20
 * Time: 16:35
 */
@XStreamAlias("xml")
@Data
public class NewsMessage {

    @XStreamAlias("ToUserName")
    private String toUserName;

    @XStreamAlias("FromUserName")
    private String fromUserName;

    @XStreamAlias("CreateTime")
    private long createTime;

    @XStreamAlias("MsgType")
    private String msgType;

    @XStreamAlias("ArticleCount")
    private Integer articleCount;

    @XStreamAlias("Articles")
    private List<Article> articles;


    public static NewsMessage getReplyNewsMessage(Map<String, Object> map) {
        NewsMessage newsMessage = new NewsMessage();
        newsMessage.setToUserName((String) map.get("FromUserName"));
        newsMessage.setFromUserName((String) map.get("ToUserName"));
        newsMessage.setCreateTime(System.currentTimeMillis() / 1000);
        newsMessage.setMsgType("news");
        newsMessage.setArticleCount(1);
//        newsMessage.setArticles(new ArrayList<Article>() {{
//            this.add(new Article(){{
//                this.setTitle("文1");
//                this.setDescription("描述1");
//                this.setUrl("https://blog.csdn.net/Carefree_State?type=blog");
//                this.setPicUrl("http://mmbiz.qpic.cn/sz_mmbiz_jpg/M6lc7Lf7u4jOOQRVia3zia334d7FIXC4DoJ984J6kCqicfIaCMsXrqvjJ9ylmNOq25vHPOgv9t0lUva50iapUd5Cpg/0");
//            }});
//        }});
        Article article = new Article();
        article.setTitle("文1");
        article.setDescription("描述1");
        article.setPicUrl("http://mmbiz.qpic.cn/sz_mmbiz_jpg/M6lc7Lf7u4jOOQRVia3zia334d7FIXC4DoJ984J6kCqicfIaCMsXrqvjJ9ylmNOq25vHPOgv9t0lUva50iapUd5Cpg/0");
        article.setUrl("https://blog.csdn.net/Carefree_State?type=blog");
        List<Article> articleList = new ArrayList<>();
        articleList.add(article);
        newsMessage.setArticles(articleList);
        System.out.println(newsMessage);
        return newsMessage;
    }
}
